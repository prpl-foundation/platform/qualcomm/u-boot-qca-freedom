# Contributing

Follow the official [Contribution Guidelines for prpl Open-Source Projects](https://gitlab.com/groups/prpl-foundation/-/wikis/Contributing).

Additionally make sure, that you document **all** changes in
[CHANGELOG.md](CHANGELOG.md) file and thus help maintain the release notes.
