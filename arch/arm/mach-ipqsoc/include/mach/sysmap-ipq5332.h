/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * (C) Copyright 2022 Sumit Garg <sumit.garg@linaro.org>
 *
 * Copyright (c) 2023, Qualcomm Innovation Center, Inc. All rights reserved.
 *
 */
#ifndef _MACH_SYSMAP_IPQ5332_H
#define _MACH_SYSMAP_IPQ5332_H

#define SDCC1_SRC_SEL_GPLL2_OUT_MAIN	(2 << 8)
#define SDCC1_SRC_SEL_GPLL0_OUT_MAIN	(1 << 8)
#define PCIE_SRC_SEL_XO			(0 << 8)
#define PCIE_SRC_SEL_GPLL4_OUT_MAIN	(2 << 8)
#define PCIE_SRC_SEL_GPLL0_OUT_MAIN	(1 << 8)

/* BLSP UART clock register */
#define BLSP1_AHB_CBCR			0x01008
#define BLSP1_UART1_BCR			0x02028
#define APCS_CLOCK_BRANCH_ENA_VOTE	0x0B004

#define BLSP1_UART_BCR(id)		((id < 1) ? \
					(BLSP1_UART1_BCR):\
					(BLSP1_UART1_BCR + (0x1000 * id)))

#define BLSP1_UART_APPS_CMD_RCGR(id)	(BLSP1_UART_BCR(id) + 0x04)
#define BLSP1_UART_APPS_CFG_RCGR(id)	(BLSP1_UART_BCR(id) + 0x08)
#define BLSP1_UART_APPS_M(id)		(BLSP1_UART_BCR(id) + 0x0c)
#define BLSP1_UART_APPS_N(id)		(BLSP1_UART_BCR(id) + 0x10)
#define BLSP1_UART_APPS_D(id)		(BLSP1_UART_BCR(id) + 0x14)
#define BLSP1_UART_APPS_CBCR(id)	(BLSP1_UART_BCR(id) + 0x18)

/* Uart clock control registers */
#define BLSP1_UART2_BCR			(0x3028)
#define BLSP1_UART2_APPS_CBCR		(0x302C)
#define BLSP1_UART2_APPS_CMD_RCGR	(0x3034)
#define BLSP1_UART2_APPS_CFG_RCGR	(0x3038)
#define BLSP1_UART2_APPS_M		(0x303C)
#define BLSP1_UART2_APPS_N		(0x3040)
#define BLSP1_UART2_APPS_D		(0x3044)

/* BLSP QUP SPI clock register */
#define BLSP1_QUP1_SPI_BCR		0x02000

#define BLSP1_QUP_SPI_BCR(id)		((id < 1) ? \
					(BLSP1_QUP1_SPI_BCR):\
					(BLSP1_QUP1_SPI_BCR + (0x1000 * id)))

#define BLSP1_QUP_SPI_APPS_CMD_RCGR(id)	(BLSP1_QUP_SPI_BCR(id) + 0x04)
#define BLSP1_QUP_SPI_APPS_CFG_RCGR(id)	(BLSP1_QUP_SPI_BCR(id) + 0x08)
#define BLSP1_QUP_SPI_APPS_M(id)	(BLSP1_QUP_SPI_BCR(id) + 0x0c)
#define BLSP1_QUP_SPI_APPS_N(id)	(BLSP1_QUP_SPI_BCR(id) + 0x10)
#define BLSP1_QUP_SPI_APPS_D(id)	(BLSP1_QUP_SPI_BCR(id) + 0x14)
#define BLSP1_QUP_SPI_APPS_CBCR(id)	(BLSP1_QUP_SPI_BCR(id) + 0x20)

#define BLSP1_QUP_SPI_SRC_SEL_GPLL0_OUT_MAIN		(1 << 8)

/* BLSP QUP I2C clock register */
#define BLSP1_QUP1_I2C_BCR		0x02000

#define BLSP1_QUP_I2C_BCR(id)		((id < 1) ? \
					(BLSP1_QUP1_I2C_BCR):\
					(BLSP1_QUP1_I2C_BCR + (0x1000 * id)))

#define BLSP1_QUP_I2C_APPS_CMD_RCGR(id)	(BLSP1_QUP_I2C_BCR(id) + 0x18)
#define BLSP1_QUP_I2C_APPS_CFG_RCGR(id)	(BLSP1_QUP_I2C_BCR(id) + 0x1C)
#define BLSP1_QUP_I2C_APPS_CBCR(id)	(BLSP1_QUP_I2C_BCR(id) + 0x24)

#define BLSP1_QUP_I2C_50M_DIV_VAL	(0x1F << 0)
#define BLSP1_QUP_I2C_SRC_SEL_GPLL0_OUT_MAIN		(1 << 8)

/* SD controller clock control registers */
#define SDCC1_BCR			(0x33000)
#define SDCC1_APPS_CMD_RCGR		(0x33004)
#define SDCC1_APPS_CFG_RCGR		(0x33008)
#define SDCC1_APPS_M			(0x3300C)
#define SDCC1_APPS_N			(0x33010)
#define SDCC1_APPS_D			(0x33014)
#define SDCC1_APPS_CBCR			(0x3302C)
#define GCC_SDCC1_AHB_CBCR		(0x33034)

/* USB clock control registers */
#define GCC_USB0_MASTER_CBCR		(0x2C048)
#define GCC_USB0_SLEEP_CBCR		(0x2C058)
#define GCC_USB0_MOCK_UTMI_CBCR		(0x2C054)
#define GCC_USB0_PIPE_CBCR		(0x2C078)
#define GCC_USB0_PHY_CFG_AHB_CBCR	(0x2C05C)
#define GCC_USB0_AUX_CBCR		(0x2C050)
#define GCC_USB0_LFPS_CBCR		(0x2C090)

#define GCC_USB0_MASTER_CMD_RCGR	(0x2C004)
#define GCC_USB0_MASTER_CFG_RCGR	(0x2C008)
#define GCC_USB0_MOCK_UTMI_CMD_RCGR	(0x2C02C)
#define GCC_USB0_MOCK_UTMI_CFG_RCGR	(0x2C030)
#define GCC_USB0_MOCK_UTMI_M		(0x2C034)
#define GCC_USB0_MOCK_UTMI_N		(0x2C038)
#define GCC_USB0_MOCK_UTMI_D		(0x2C03C)
#define GCC_USB0_AUX_CMD_RCGR		(0x2C018)
#define GCC_USB0_AUX_CFG_RCGR		(0x2C01C)
#define GCC_USB0_LFPS_CMD_RCGR		(0x2C07C)
#define GCC_USB0_LFPS_CFG_RCGR		(0x2C080)
#define GCC_USB0_LFPS_M			(0x2C084)
#define GCC_USB0_LFPS_N			(0x2C088)
#define GCC_USB0_LFPS_D			(0x2C08C)

#define USB0_MASTER_SRC_SEL_GPLL0_OUT_MAIN	(1 << 8)
#define USB0_MOCK_UTMI_SRC_SEL_GPLL4_OUT_AUX	(1 << 8)
#define USB0_AUX_CFG_SRC_SEL_XO			(0 << 8)
#define USB0_LFPS_CFG_SRC_SEL_GPLL0_OUT_MAIN	(0x1 << 8)

/* GCC clock control registers */
#define GCC_MDIO_MASTER_AHB_CBCR	(0x12004)
#define GCC_UNIPHY0_SYS_CBCR		(0x1600C)
#define GCC_UNIPHY0_AHB_CBCR		(0x16010)
#define GCC_UNIPHY1_SYS_CBCR		(0x16018)
#define GCC_UNIPHY1_AHB_CBCR		(0x1601C)
#define GCC_NSSNOC_ATB_CBCR		(0x17014)
#define GCC_NSSNOC_QOSGEN_REF_CBCR	(0x1701C)
#define GCC_NSSNOC_TIMEOUT_REF_CBCR	(0x17020)
#define GCC_NSSNOC_SNOC_CBCR		(0x17028)
#define GCC_NSSCFG_CBCR			(0x1702C)
#define GCC_NSSNOC_NSSCC_CBCR		(0x17030)
#define GCC_NSSCC_CBCR			(0x17034)
#define GCC_NSSNOC_SNOC_1_CBCR		(0x1707C)
#define GCC_QDSS_AT_CBCR		(0x2D038)
#define GCC_IM_SLEEP_CBCR		(0x34020)
#define GCC_CMN_12GPLL_AHB_CBCR		(0x3A004)
#define GCC_CMN_12GPLL_SYS_CBCR		(0x3A008)

#define GCC_QDSS_AT_CMD_RCGR		(0x2D004)
#define GCC_QDSS_AT_CFG_RCGR		(0x2D008)
#define GCC_SYSTEM_NOC_BFDCD_CMD_RCGR	(0x2E004)
#define GCC_SYSTEM_NOC_BFDCD_CFG_RCGR	(0x2E008)
#define GCC_PCNOC_BFDCD_CMD_RCGR	(0x31004)
#define GCC_PCNOC_BFDCD_CFG_RCGR	(0x31008)

#define PCCNOC_BFDCD_SRC_SEL_GPLL0_OUT_MAIN		(1 << 8)
#define QDSS_SRC_SEL_GPLL4_OUT_MAIN			(1 << 8)
#define GCC_SYSTEM_NOC_BFDCD_SRC_SEL_GPLL4_OUT_MAIN	(2 << 8)
/*
 * PCIE
 * PCIE0 ---> PCIE3X1_0
 * PCIE1 ---> PCIE3X2
 * PCIE2 ---> PCIE3X1_1
 */
#define GCC_CONFIG_PCIE0			0
#define GCC_CONFIG_PCIE1			1
#define GCC_CONFIG_PCIE2			2

#define GCC_SNOC_PCIE3_2LANE_S_CBCR		0x2E048
#define GCC_SNOC_PCIE3_1LANE_S_CBCR		0x2E04C
#define GCC_SNOC_PCIE3_1LANE_1_M_CBCR		0x2E050
#define GCC_SNOC_PCIE3_2LANE_M_CBCR		0x2E07C
#define GCC_SNOC_PCIE3_1LANE_M_CBCR		0x2E080
#define GCC_SNOC_PCIE3_1LANE_1_S_CBCR		0x2E0AC

#define GCC_PCIE3X2_BASE			0x28000
#define GCC_PCIE3X1_0_BASE			0x29000
#define GCC_PCIE3X1_1_BASE			0x2A000

#define GCC_PCIE_AUX_CMD_RCGR			0x28004
#define GCC_PCIE_AUX_CFG_RCGR			0x28008
#define GCC_PCIE_AUX_M				0x2800C
#define GCC_PCIE_AUX_N				0x28010
#define GCC_PCIE_AUX_D				0x28014

#define GCC_PCIE3X2_BCR				(GCC_PCIE3X2_BASE+0x000)
#define GCC_PCIE3X2_AXI_M_CMD_RCGR		(GCC_PCIE3X2_BASE+0x018)
#define GCC_PCIE3X2_AXI_M_CFG_RCGR		(GCC_PCIE3X2_BASE+0x01C)
#define GCC_PCIE3X2_AHB_CBCR			(GCC_PCIE3X2_BASE+0x030)
#define GCC_PCIE3X2_AXI_M_CBCR			(GCC_PCIE3X2_BASE+0x038)
#define GCC_PCIE3X2_AXI_M_SREGR			(GCC_PCIE3X2_BASE+0x03C)
#define GCC_PCIE3X2_AXI_S_CBCR			(GCC_PCIE3X2_BASE+0x040)
#define GCC_PCIE3X2_AXI_S_SREGR			(GCC_PCIE3X2_BASE+0x044)
#define GCC_PCIE3X2_AXI_S_BRIDGE_CBCR		(GCC_PCIE3X2_BASE+0x048)
#define GCC_PCIE3X2_PIPE_CBCR			(GCC_PCIE3X2_BASE+0x068)
#define GCC_PCIE3X2_AUX_CBCR			(GCC_PCIE3X2_BASE+0x070)
#define GCC_PCIE3X2_RCHG_CMD_RCGR		(GCC_PCIE3X2_BASE+0x078)
#define GCC_PCIE3X2_RCHG_CFG_RCGR		(GCC_PCIE3X2_BASE+0x07C)
#define GCC_PCIE3X2_PHY_AHB_CBCR		(GCC_PCIE3X2_BASE+0x080)
#define GCC_PCIE3X2_AXI_S_CMD_RCGR		(GCC_PCIE3X2_BASE+0x084)
#define GCC_PCIE3X2_AXI_S_CFG_RCGR		(GCC_PCIE3X2_BASE+0x088)

#define GCC_PCIE3X1_0_BCR			(GCC_PCIE3X1_0_BASE+0x000)
#define GCC_PCIE3X1_0_AXI_CMD_RCGR		(GCC_PCIE3X1_0_BASE+0x018)
#define GCC_PCIE3X1_0_AXI_CFG_RCGR		(GCC_PCIE3X1_0_BASE+0x01C)
#define GCC_PCIE3X1_0_AHB_CBCR			(GCC_PCIE3X1_0_BASE+0x030)
#define GCC_PCIE3X1_0_AXI_M_CBCR		(GCC_PCIE3X1_0_BASE+0x038)
#define GCC_PCIE3X1_0_AXI_M_SREGR		(GCC_PCIE3X1_0_BASE+0x03C)
#define GCC_PCIE3X1_0_AXI_S_CBCR		(GCC_PCIE3X1_0_BASE+0x040)
#define GCC_PCIE3X1_0_AXI_S_SREGR		(GCC_PCIE3X1_0_BASE+0x044)
#define GCC_PCIE3X1_0_AXI_S_BRIDGE_CBCR		(GCC_PCIE3X1_0_BASE+0x048)
#define GCC_PCIE3X1_0_PIPE_CBCR			(GCC_PCIE3X1_0_BASE+0x068)
#define GCC_PCIE3X1_0_AUX_CBCR			(GCC_PCIE3X1_0_BASE+0x070)
#define GCC_PCIE3X1_PHY_AHB_CBCR		(GCC_PCIE3X1_0_BASE+0x078)
#define GCC_PCIE3X1_0_RCHG_CMD_RCGR		(GCC_PCIE3X1_0_BASE+0x07C)
#define GCC_PCIE3X1_0_RCHG_CFG_RCGR		(GCC_PCIE3X1_0_BASE+0x080)

#define GCC_PCIE3X1_1_BCR			(GCC_PCIE3X1_1_BASE+0x000)
#define GCC_PCIE3X1_1_AXI_CMD_RCGR		(GCC_PCIE3X1_1_BASE+0x004)
#define GCC_PCIE3X1_1_AXI_CFG_RCGR		(GCC_PCIE3X1_1_BASE+0x008)
#define GCC_PCIE3X1_1_AHB_CBCR			(GCC_PCIE3X1_1_BASE+0x00C)
#define GCC_PCIE3X1_1_AXI_M_CBCR		(GCC_PCIE3X1_1_BASE+0x014)
#define GCC_PCIE3X1_1_AXI_M_SREGR		(GCC_PCIE3X1_1_BASE+0x018)
#define GCC_PCIE3X1_1_AXI_S_CBCR		(GCC_PCIE3X1_1_BASE+0x01C)
#define GCC_PCIE3X1_1_AXI_S_SREGR		(GCC_PCIE3X1_1_BASE+0x020)
#define GCC_PCIE3X1_1_AXI_S_BRIDGE_CBCR		(GCC_PCIE3X1_1_BASE+0x024)
#define GCC_PCIE3X1_1_PIPE_CBCR			(GCC_PCIE3X1_1_BASE+0x068)
#define GCC_PCIE3X1_1_AUX_CBCR			(GCC_PCIE3X1_1_BASE+0x070)
#define GCC_PCIE3X1_1_AUX_SREGR			(GCC_PCIE3X1_1_BASE+0x074)
#define GCC_PCIE3X1_1_RCHG_CMD_RCGR		(GCC_PCIE3X1_1_BASE+0x078)
#define GCC_PCIE3X1_1_RCHG_CFG_RCGR		(GCC_PCIE3X1_1_BASE+0x07C)

/* NSS clock control registers */
#define NSS_CC_PPE_SWITCH_IPE_CBCR	(0x003F8)
#define NSS_CC_PPE_SWITCH_BTQ_CBCR	(0x00400)
#define NSS_CC_PPE_SWITCH_CBCR		(0x00408)
#define NSS_CC_PPE_SWITCH_CFG_CBCR	(0x00410)
#define NSS_CC_PPE_EDMA_CBCR		(0x00414)
#define NSS_CC_PPE_EDMA_CFG_CBCR	(0x0041C)
#define NSS_CC_NSSNOC_PPE_CBCR		(0x00420)
#define NSS_CC_NSSNOC_PPE_CFG_CBCR	(0x00424)
#define NSS_CC_PORT1_MAC_CBCR		(0x00428)
#define NSS_CC_PORT2_MAC_CBCR		(0x00430)
#define NSS_CC_PORT1_RX_CBCR		(0x00480)
#define NSS_CC_PORT1_TX_CBCR		(0x00488)
#define NSS_CC_PORT2_RX_CBCR		(0x00490)
#define NSS_CC_PORT2_TX_CBCR		(0x00498)
#define NSS_CC_UNIPHY_PORT1_RX_CBCR	(0x004B4)
#define NSS_CC_UNIPHY_PORT1_TX_CBCR	(0x004B8)
#define NSS_CC_UNIPHY_PORT2_RX_CBCR	(0x004BC)
#define NSS_CC_UNIPHY_PORT2_TX_CBCR	(0x004C0)
#define NSS_CC_NSS_CSR_CBCR		(0x005E8)
#define NSS_CC_NSSNOC_NSS_CSR_CBCR	(0x005EC)

#define NSS_CC_PPE_CMD_RCGR		(0x003E8)
#define NSS_CC_PPE_CFG_RCGR		(0x003EC)
#define NSS_CC_CFG_CMD_RCGR		(0x005E0)
#define NSS_CC_CFG_CFG_RCGR		(0x005E4)
#define NSS_CC_PORT1_RX_CMD_RCGR	(0x00450)
#define NSS_CC_PORT1_RX_CFG_RCGR	(0x00454)
#define NSS_CC_PORT1_RX_DIV_CDIVR	(0x00458)
#define NSS_CC_PORT1_TX_CMD_RCGR	(0x0045C)
#define NSS_CC_PORT1_TX_CFG_RCGR	(0x00460)
#define NSS_CC_PORT1_TX_DIV_CDIVR	(0x00464)
#define NSS_CC_PORT2_RX_CMD_RCGR	(0x00468)
#define NSS_CC_PORT2_RX_CFG_RCGR	(0x0046C)
#define NSS_CC_PORT2_RX_DIV_CDIVR	(0x00470)
#define NSS_CC_PORT2_TX_CMD_RCGR	(0x00474)
#define NSS_CC_PORT2_TX_CFG_RCGR	(0x00478)
#define NSS_CC_PORT2_TX_DIV_CDIVR	(0x0047C)

#define NSS_CC_SRC_SEL_GCC_GPLL0_OUT_AUX		(2 << 8)
#define NSS_CC_PPE_SRC_SEL_CMN_PLL_NSS_CLK_200M		(6 << 8)
#define NSS_CC_PORT_RX_SRC_SEL_UNIPHY_NSS_RX_CLK	(3 << 8)
#define NSS_CC_PORT_TX_SRC_SEL_UNIPHY_NSS_TX_CLK	(4 << 8)

#define CLK_2_5_MHZ			(2500000UL)
#define CLK_25_MHZ			(25000000UL)
#define CLK_78_125_MHZ			(78125000UL)
#define CLK_50_MHZ			(50000000UL)
#define CLK_125_MHZ			(125000000UL)
#define CLK_156_25_MHZ			(156250000UL)
#define CLK_312_5_MHZ			(312500000UL)

/*
 * QTI SPI NAND clock
 */
#define GCC_QPIC_IO_MACRO_CMD_RCGR	(0x32004)
#define GCC_QPIC_IO_MACRO_CFG_RCGR	(0x32008)
#define GCC_QPIC_IO_MACRO_CBCR		(0x3200C)

#define IO_MACRO_CLK_320_MHZ		(320000000)
#define IO_MACRO_CLK_266_MHZ		(266000000)
#define IO_MACRO_CLK_228_MHZ		(228000000)
#define IO_MACRO_CLK_200_MHZ		(200000000)
#define IO_MACRO_CLK_100_MHZ		(100000000)
#define IO_MACRO_CLK_24_MHZ		(24000000)

#define GCC_QPIC_IO_MACRO_SRC_SEL_XO_CLK		(0 << 8)
#define GCC_QPIC_IO_MACRO_SRC_SEL_GPLL0_OUT_MAIN	(1 << 8)

#endif
