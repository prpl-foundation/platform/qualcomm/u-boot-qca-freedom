/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2023, Qualcomm Innovation Center, Inc. All rights reserved.
 */

#ifndef _DT_BINDINGS_CLK_GCC_DEVSOC_H
#define _DT_BINDINGS_CLK_GCC_DEVSOC_H

/* GCC controlled clock IDs */
#define GCC_QUPV3_UART0_CLK				0
#define GCC_QUPV3_UART1_CLK				1
#define GCC_QPIC_IO_MACRO_CLK				2
#define GCC_QUPV3_SE2_CLK				3
#define GCC_QUPV3_SE3_CLK				4
#define GCC_QUPV3_SE4_CLK				5
#define GCC_QUPV3_SE5_CLK				6
#define GCC_SDCC1_APPS_CLK				7
#define GCC_SDCC1_AHB_CLK				8

#endif
