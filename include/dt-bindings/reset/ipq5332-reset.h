/* Copyright (c) 2015 The Linux Foundation. All rights reserved.
 *
 * Copyright (c) 2023, Qualcomm Innovation Center, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#ifndef __IPQ5332_RESET_H__
#define __IPQ5332_RESET_H__

#define GCC_SDCC1_BCR					0
#define GCC_UNIPHY0_BCR					1
#define GCC_UNIPHY1_BCR					2
#define GCC_UNIPHY0_SOFT_RESET				3
#define GCC_UNIPHY1_SOFT_RESET				4
#define GCC_UNIPHY0_XPCS_RESET				5
#define GCC_UNIPHY1_XPCS_RESET				6
#define NSS_CC_PPE_BCR					7
#define NSS_CC_PORT1_RX_RESET				8
#define NSS_CC_PORT1_TX_RESET				9
#define NSS_CC_PORT2_RX_RESET				10
#define NSS_CC_PORT2_TX_RESET				11
#define GCC_PCIE3X1_0_BCR_RESET				12
#define GCC_PCIE3X1_1_BCR_RESET				13
#define GCC_PCIE3X2_BCR_RESET				14
#define GCC_PCIE3X2_PHY_BCR_RESET			14
#define GCC_PCIE3X1_0_PHY_BCR_RESET			15
#define GCC_PCIE3X1_1_PHY_BCR_RESET			16
#define GCC_USB_BCR					17
#define GCC_QUSB2_0_PHY_BCR				18
#define GCC_USB0_PHY_BCR				19

#endif
