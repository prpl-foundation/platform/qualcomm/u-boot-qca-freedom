/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (c) 2023, Qualcomm Innovation Center, Inc. All rights reserved.
 */

#ifndef _DT_BINDINGS_PINCTRL_IPQSOC_H
#define _DT_BINDINGS_PINCTRL_IPQSOC_H

/* GPIO Drive Strength */
#define DRIVE_STRENGTH_2MA        0
#define DRIVE_STRENGTH_4MA        1
#define DRIVE_STRENGTH_6MA        2
#define DRIVE_STRENGTH_8MA        3
#define DRIVE_STRENGTH_10MA       4
#define DRIVE_STRENGTH_12MA       5
#define DRIVE_STRENGTH_14MA       6
#define DRIVE_STRENGTH_16MA       7

#endif
