/*
 **************************************************************************
 * Copyright (c) 2016-2019, 2021, The Linux Foundation. All rights reserved.
 *
 * Copyright (c) 2022-2023, Qualcomm Innovation Center, Inc. All rights reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 **************************************************************************
*/

#ifndef _DT_BINDINGS_QTI_IPQSOC_H
#define _DT_BINDINGS_QTI_IPQSOC_H

#define QCA8075_PHY_TYPE		0
#define QCA8081_PHY_TYPE		1
#define AQ_PHY_TYPE			2
#define QCA8033_PHY_TYPE		3
#define SFP_PHY_TYPE			4
#define SFP10G_PHY_TYPE			5
#define SFP2_5G_PHY_TYPE		6
#define SFP1G_PHY_TYPE			7
#define QCA8x8x_PHY_TYPE		8
#define QCA8337_SWITCH_TYPE		9
#define QCA8x8x_SWITCH_TYPE		10
#define QCA8x8x_BYPASS_TYPE		11
#define UNUSED_PHY_TYPE			0xFF

#endif

