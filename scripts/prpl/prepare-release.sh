#!/usr/bin/env bash

release_version=$1

[ -n "$release_version" ] || {
	cat <<-EOT
	Usage: $0 <release version> [upstream version]
	EOT

	exit 1
}

set -eu
release_date=$(date +"%Y-%m-%d")
release_version=$(printf "%02d" "${release_version}")
upstream_version=${2:-2023.04}
release_tag="${upstream_version}.${release_version}-prpl"
release_version_next="$(( 10#$release_version+1 ))"
release_tag_next=$(printf "%s.%02d-prpl" "${upstream_version}" "${release_version_next}")

cleanup()
{
	trap - EXIT HUP INT QUIT ABRT ALRM TERM

	if [ -n "${initial_branch:-}" ] && \
		[ "$(git rev-parse --abbrev-ref HEAD)" != "${initial_branch:-}" ]; then
		git switch "${initial_branch}"
	fi
}

init()
{
	initial_branch="$(git rev-parse --abbrev-ref HEAD)"
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM
}

create_release()
{
	git switch --force-create "prpl/make-release-${release_tag}"

	sed -i "s/^SUBLEVEL =$/SUBLEVEL = ${release_version}/" Makefile
	sed -i "s|^## Release $release_tag (unreleased)|## Release $release_tag (released ${release_date})|" CHANGELOG.md
	awk '/^$/ {if (i) {b=b $0 "\n"} else {print $0 }; next} /^## / {i=0; print $0; next} /^###/ {i=1; b=$0; next} {if (i) {print b}; i=0; print $0; next}' \
		CHANGELOG.md > _CHANGELOG.md && mv _CHANGELOG.md CHANGELOG.md
	sed -i '${/^$/d;}' CHANGELOG.md

	lr="Latest release is [${release_tag}](https://gitlab.com/prpl-foundation/platform/qualcomm/u-boot-qca-freedom/-/releases/${release_tag})"
	sed -i "s|^Latest release is .*$|${lr}|" README.md
	sed -i "/^Latest release is/{n;s|^released [0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}\.$|released ${release_date}.|;}" README.md
	sed -i "s|git checkout 2023.04.*|git checkout ${release_tag}|" README.md

	date +"%s" | tee version.date
	git add version.date

	git commit \
		--signoff \
		--message "prpl: make $release_tag release" \
		--message "This is automatically generated commit using ${0}." \
		Makefile \
		CHANGELOG.md \
		README.md \
		version.date

	git tag \
		--sign \
		--force \
		--message "U-Boot for Freedom $release_tag release" \
		"$release_tag"
}

mark_next_release()
{
	local tmp_file

	git switch --force-create "prpl/mark-next-release-${release_tag_next}"

	tmp_file=$(mktemp)

cat <<EOF > "$tmp_file"
# Changelog

## Release $release_tag_next (unreleased)

### New features in $release_tag_next

### Bug fixes in $release_tag_next

### Breaking changes in $release_tag_next

### Known issues in $release_tag_next

EOF

	sed '1,2d' CHANGELOG.md > _CHANGELOG.md
	cat "$tmp_file" _CHANGELOG.md > CHANGELOG.md

	sed -i "s/^SUBLEVEL = ${release_version}$/SUBLEVEL =/" Makefile

	git rm version.date
	git commit \
		--signoff \
		--message "prpl: start working on next release $release_tag_next" \
		--message "This is automatically generated commit using ${0}." \
		CHANGELOG.md \
		Makefile \
		version.date
}

init
create_release
mark_next_release
cleanup
