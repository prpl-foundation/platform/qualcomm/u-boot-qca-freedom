# License

Follow the upstream [README](Licenses/README) for licensing details.

U-Boot is Free Software. It is copyrighted by Wolfgang Denk and many others
who contributed code (see the actual source code and the git commit messages
for details).  You can redistribute U-Boot and/or modify it under the terms of
version 2 of the GNU General Public License as published by the Free Software
Foundation.
