# U-Boot for the prpl Foundation/WNC Freedom platform

Goal of this project is to provide source code and binary releases for the
U-Boot bootloader as being shipped with the reference platform devices or
otherwise distributed within prpl Foundation.

Latest release is [2023.04.01-prpl](https://gitlab.com/prpl-foundation/platform/qualcomm/u-boot-qca-freedom/-/releases/2023.04.01-prpl)
released 2024-06-26.

## Building

### Using Ubuntu 22.04 build host

#### Installing build dependencies

```sh
sudo apt-get update
sudo apt-get install --yes \
             bc bison flex gcc git gpg libbsd-dev \
             libssl-dev lzop make python2 wget xz-utils
```

#### Building U-Boot

The goal of this step is to produce `u-boot.elfmbn` bootloader binary which
can be flashed from within U-Boot itself.

```sh
git clone https://gitlab.com/prpl-foundation/platform/qualcomm/u-boot-qca-freedom
cd u-boot-qca-freedom
make
git checkout 2023.04.01-prpl
make PYTHON=python2
```

*Note: the build is limited to a single make job intentionally for reproducibility.*

## Installation

### Update from U-Boot version 2016.01 to 2023.04

If your currently running U-Boot version is 2016.01, please use the
[emmc-ipq9574_64-single-freedom-u-boot-2023.04-20240605.img](https://drive.google.com/drive/folders/1GtN1Sz52i7w8E2iZ8bx0Hp4vV1nzUNsj)
to update the partitions to those needed by U-Boot 2023.04 using commands bellow.

```sh
tftpboot 0x44000000 emmc-ipq9574_64-single-freedom-u-boot-2023.04-20240605.img
imgaddr=0x44000000 && source $imgaddr:script
reset
```

### Flashing using TFTP method

You can flash `u-boot.elfmbn` U-Boot binary from the U-Boot using following commands.

```sh
tftpboot 0x44000000 u-boot.elfmbn
mmc erase 0x4a22 0x600
mmc write 0x44000000 0x4a22 0x600
reset
```

### eMMC flash layout

Freedom board comes preloaded from factory with the following EFI partition
layout on the eMMC flash memory.

| Part# | Start LBA | End LBA |  Size   |    Name     | Description |
| ----- | --------- | ------- | ------- | ----------- | ----------- |
|  17   |   0x4822  |  0x4a21 | 0x1ff   | APPSBLENV   | U-Boot environment |
|  18   |   0x4a22  |  0x5021 | 0x600   | APPSBL      | U-Boot |

### Recovery

## Contributing

Follow the [contribution guidelines](CONTRIBUTING.md).

## Releases

Latest release is [2023.04.01-prpl](https://gitlab.com/prpl-foundation/platform/qualcomm/u-boot-qca-freedom/-/releases/2023.04.01-prpl)
released 2024-06-26.

You can find past releases on the [release page](https://gitlab.com/prpl-foundation/platform/qualcomm/u-boot-qca-freedom/-/releases).
Changelog for the releases is available in [CHANGELOG.md](CHANGELOG.md).

### Versioning scheme

Currently the plan is to follow upstream based versioning scheme in the
form of `2023.04.yy-prpl` where:

* `2023.04` is the upstream release used as a base
* `yy` is two digit number used to mark `prpl` releases
* `-prpl` is fixed text to make it clear, that its `prpl` related U-Boot

### Creating releases

Lets say, that we're currently working on `2023.04.05-prpl` release, then we
can actually make that release by issuing following command:

```sh
./scripts/prpl/prepare-release.sh 5
```

which will create release notes and `2023.04.05-prpl` signed release Git tag
including following branches:

* prpl/make-release-2023.04.05-prpl
* prpl/mark-next-release-2023.04.06-prpl

which can then be used in the following manner:

1. Push prepared release `git push origin prpl/make-release-2023.04.05-prpl`.
1. Create a merge request, flash U-Boot and check if everything works as
   expected.
1. Merge the prepared release.
1. Push the tag with `git push origin 2023.04.05-prpl` and let CI prepare
   release.
1. Push preparation for next release `git push origin prpl/mark-next-release-2023.04.06-prpl`.
1. Once verified, merge it so the work on next `2023.04.06-prpl` release can start.

## Branches

| branch | description |
| ------ | ----------- |
| `prpl/2016.01` | Development and release branch based on Qualcomm's [u-boot-2016](https://git.codelinaro.org/clo/qsdk/oss/boot/u-boot-2016)|
| `prpl/2023.04` | Development and release branch based on Qualcomm's [u-boot](https://git.codelinaro.org/clo/qsdk/oss/boot/u-boot)|

## Repository content origin

Origin of this repository was created using following commands:

```sh
git clone https://git.codelinaro.org/clo/qsdk/oss/boot/u-boot && cd u-boot
git reset --hard 14eef79ef981e47426ecb99d796ac9a18efb27c2
```

Then we've applied fixes and various WNC patches delivered for Freedom board support.
