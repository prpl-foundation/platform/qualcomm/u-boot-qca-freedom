// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (c) 2023, Qualcomm Innovation Center, Inc. All rights reserved.
 */

#ifndef _QTI_H_
#define _QTI_H_

#include <linux/compat.h>

int phy_8x8x_init(void);
int phy_8075_init(void);
int phy_8081_init(void);

#endif                          /* _QTI_H_ */
