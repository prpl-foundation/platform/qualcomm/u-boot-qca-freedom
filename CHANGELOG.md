# Changelog

## Release 2023.04.02-prpl (unreleased)

### New features in 2023.04.02-prpl

### Bug fixes in 2023.04.02-prpl

### Breaking changes in 2023.04.02-prpl

### Known issues in 2023.04.02-prpl

## Release 2023.04.01-prpl (released 2024-06-26)

### New features in 2023.04.01-prpl

* It's now possible to build U-Boot using single `make` command.
* U-Boot version now contains `-prpl` suffix so its clear, that its U-Boot
  with QCA/WNC/prpl related changes.
* U-Boot is now being built with OpenWrt 23.05.3 SDK using GCC version 12.3.0
* Add initial support for Freedom board.
* Add support for `untar` command which makes flashing of OpenWrt sysupgrade
  images easier.
* Added flashing instructions needed for updating of U-Boot version from
  2016.01 to 2023.04.
* Build system now defaults to Python3.

### Bug fixes in 2023.04.01-prpl

* Backported upstream fix commit 8d4f11c203 ("bootm: fix size arg of
  flush_cache() in bootm_load_os()") which fixes possible hang of CPU due to
  access of address beyond valid memory range.
* Fix LAN and WAN network unusable without Aquantia PHY reset (PCF-1178).
* Fix wan port MAC address get random value.
